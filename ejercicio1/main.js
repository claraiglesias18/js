const tarea = document.getElementById("tarea");
const agregar = document.getElementById("agregar");
const lista = document.getElementById("lista");
const todo = document.getElementById("todo");
let tareas = [];

if (localStorage.length !== 0) {
    
    imprimir();

};


//imprimir LocalStorage y actualizar el array de tareas

function imprimir() {

    let array = localStorage.getItem("tareas");

    array = JSON.parse(array);

    array.forEach(element => {
        
        tareas.push(

            {nombre: element.nombre, prioridad: element.prioridad}

        );
        
        const li = document.createElement("li");
        const texto = document.createTextNode(element.nombre);

        li.id = element.prioridad;

        li.appendChild(texto);
        li.appendChild(eliminar());

        lista.appendChild(li);


    });

}


//agregar tareas

agregar.addEventListener("click", (e) => {

    e.preventDefault();

    const contenido = tarea.value;       
  
    const li = document.createElement("li");
    const texto = document.createTextNode(contenido);

    li.id = document.getElementById("prioridad").value;

    //agregar a array

    tareas.push(

        {nombre: contenido, prioridad: document.getElementById("prioridad").value}
    
    );

    //actualizar LocalStorage con array de tareas
    
    localStorage.setItem("tareas", JSON.stringify(tareas));
    
    li.appendChild(texto);
    li.appendChild(eliminar());

    lista.appendChild(li);

    tarea.value = "";
    
})

//eliminar tareas

function eliminar() {

    let elimina = document.createElement("input");

    elimina.type = "button";
    elimina.value = "Eliminar";
    elimina.id = "elimina";

    elimina.addEventListener("click", (e) => {

        let eliminada = e.target.parentElement;

            let seleccion = tareas.findIndex(object => {

                return object.nombre === e.target.parentElement.textContent;

            });
        
        //eliminar de array    

        tareas.splice(seleccion, 1);

        //actualizar localStorage con array de tareas

        localStorage.setItem("tareas", JSON.stringify(tareas));

        //eliminar elemento html de tarea

        lista.removeChild(eliminada);

    })

    return elimina;


}

//eliminar todas las tareas

todo.addEventListener ("click", (e) => {

    let hijos = Array.from(lista.childNodes);
    
    //eliminar elementos html

    hijos.forEach(element => {

        lista.removeChild(element);

    });

    //eliminar array de tareas

    tareas.splice(0);

    //actualizar localStorage con array de tareas

    localStorage.setItem("tareas", JSON.stringify(tareas));

})
